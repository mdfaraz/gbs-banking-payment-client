import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";

class RegisterStatusComponent extends Component {
    render() {
        return ( 
        <div class="reg-container">
        <h3 class="reg-title-name">Registration Status</h3>
        <table class="table" >
            <tr class="col">
               <td class="row">
                  Application User Id
               </td>
               <td>
                  USERID1
               </td>
            </tr>
            <tr class="col">
               <td class="row">
                  Application Type
               </td>
               <td>
                  USERID1
               </td>
            </tr>
            <tr class="col">
               <td class="row">
                  User Id
               </td>
               <td>
                  USERID1
               </td>
            </tr>
            <tr class="col">
               <td class="row">
                  Corp Id
               </td>
               <td>
                  1244555
               </td>
            </tr>
            <tr class="col">
               <td class="row">
                  Status
               </td>
               <td>
                  Pending Self Approval
               </td>
            </tr>
        </table>
        <button type="submit" class="btn btn-primary">De-Register</button>
        </div>);
    }

}

export default RegisterStatusComponent;