import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import registrationService from "../services/registration.service";

class RegisterComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {clients: []};
    this.register = this.register.bind(this);
  }

  register(data) {
      registrationService.registerUser(data);
  }


  render() {
        return ( 
        <div class="reg-container">
            <h3 class="reg-title-name">Register</h3>
        <form class="reg-form-container">
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" class="form-control" id="inputAppUserId" value="USERID1" readOnly/>
              </div>
              <div class="form-group col-md-6">
                <input type="text" class="form-control" id="inputAppType" value="web" readOnly/>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" class="form-control" id="inputUserId" placeholder="User Id"/>
              </div>
              <div class="form-group col-md-6">
                <input type="text" class="form-control" id="inputUserName" placeholder="User Name"/>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" class="form-control" id="inputCorpId" placeholder="Corp ID"/>
              </div>
              <div class="form-group col-md-6">
                <input type="text" class="form-control" id="inputAliasId" placeholder="Alias ID"/>
              </div>
            </div>
            <button type="submit" class="btn btn-primary">Register</button>
          </form>
          </div>);
    }

}

export default RegisterComponent;