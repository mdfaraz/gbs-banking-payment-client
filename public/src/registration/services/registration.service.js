import http from '../../core/http-common'

class RegistrationService {

    registerUser(data) 
    {
        return http.post("/v1/register/user", data);
    }
    
    registerUserStatus(data) 
    {
        return http.post("/v1/register/user/status", data);
    }

    deregister(data) 
    {
        return http.post("/v1/register/user/delete", data);
    }
    
}

export default new RegistrationService();